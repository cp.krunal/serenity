#This scenrio will verify visa requirement for various country passport holder with various reasons to visit UK.
Feature: UK VISA check

  Scenario Outline: Verifying UK VISA required based on scenario
    Given Other_Country_People is on the Check if you need a UK visa page
    When he searches for "<country>"
    Then user Select reason for coming to the UK "<reason>"
    Then stay Time in UK "<stayDuration>"
    Then Are you planning to work in any type of job "<typeJobType>"
    Then select partner immigration status "<partnerImmigrationStatus>"
    Then user can see visa not required text "<visa_req_status>"

    Examples: 
      | reason                                 | stayDuration         | country   | visa_req_status    | partnerImmigrationStatus | typeJobType                  |
      | Tourism or visiting family and friends | NA                   | Australia | You will not need  | NA                       | NA                           |
      | Work, academic visit or business       | longer than 6 months | Chile     | You’ll need a visa | NA                       | Health and care professional |
      | Join partner or family for a long stay | NA                   | Colombia  | You’ll need a visa | Yes                      | NA                           |
