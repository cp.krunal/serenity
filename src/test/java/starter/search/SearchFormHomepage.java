package starter.search;

import org.openqa.selenium.By;

class SearchFormHomepage {
	static By SEARCH_FIELD_HOMEPAGE = By.name("q");
	static By COUNTRY_DDL = By.id("response");
	static By CONTINUE_BTN = By.xpath("//button[@class=\"gem-c-button govuk-button gem-c-button--bottom-margin\"]");
	static By CHECK_NEED_VISA_TITLE = By.xpath("//span[@class=\"govuk-caption-l\"]");
	static By REASON = By.xpath("//label[@class=\"gem-c-label govuk-label govuk-radios__label\"]");
	static By VISA_NOT_REQ_TEXT = By.xpath("//*[@data-flow-name='check-uk-visa']//h2[contains(@class,'heading')]");

}
