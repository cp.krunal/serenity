package starter.search;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class SearchHomepageFor extends UIInteractionSteps {

	@Step("Search for term {0}")
	public void term(String term) {
		// $(SearchFormHomepage.SEARCH_FIELD_HOMEPAGE).clear();
		// $(SearchFormHomepage.SEARCH_FIELD_HOMEPAGE).sendKeys(term, Keys.ENTER);

		Select select = new Select($(SearchFormHomepage.COUNTRY_DDL));
		select.selectByVisibleText(term);
		$(SearchFormHomepage.CONTINUE_BTN).click();
		$(SearchFormHomepage.CHECK_NEED_VISA_TITLE).isVisible();
	}

	public void reason(String reason) throws InterruptedException {
		System.out.println("***************************************************");
		List<WebElement> reasons = getDriver()
				.findElements(By.xpath("//label[@class='gem-c-label govuk-label govuk-radios__label']"));
		for (int i = 0; i < reasons.size(); i++) {
			if (reasons.get(i).getText().equals(reason)) {
				reasons.get(i).click();
				break;
			}
		}
		$(SearchFormHomepage.CONTINUE_BTN).click();
	}

	public void visaStatusNotReq(String status) {
		$(SearchFormHomepage.VISA_NOT_REQ_TEXT).getText().contains(status);
	}
}
