package starter.api;

import java.io.File;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIHelper {
	RequestSpecification request;

	public APIHelper() {
//		RestAssured.baseURI = url;
		request = RestAssured.given();
	}

	public RequestSpecification basicAuthentication(String username, String password) {
		return request.auth().preemptive().basic(username, password);
	}

	public RequestSpecification addHeader(String username, String password) {
		return basicAuthentication(username, password).header("Accept", ContentType.JSON.getAcceptHeader())
				.contentType(ContentType.JSON);
	}

	public Response postWithAuth(String username, String password) {
		return addHeader(username, password).post();
	}

	public Response post(String url, File fileName) {
		return request.contentType(ContentType.JSON).body(fileName).when().post(url);
	}

	public Response get(String url) {
		return request.contentType(ContentType.JSON).when().get(url);
	}

	public Response post(String url, String json_string) {
		System.out.println("Request ==> " + json_string);
		return request.contentType(ContentType.JSON).body(json_string).when().post(url);
	}

	public Response postWithHeader(String url, String json_string) {
		System.out.println("Request ==> " + json_string);
		return request.contentType(ContentType.JSON)
				.header("Authorization", "Basic YXBwbGljYXRpb25MZXZlbFVzZXI6QCMkRkFEU0BAIyRBRg==").body(json_string)
				.when().post(url);
	}

	public String getResponseBody(Response response) {
		return response.getBody().asString();
	}

	public int getResponseCode(Response response) {
		return response.getStatusCode();
	}

	public String getDescription(Response response) {
		System.out.println("Response ==> " + getResponseBody(response));
		String description = response.getBody().path("description").toString();
		return description;
	}

}
