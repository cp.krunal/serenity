package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.navigation.NavigateTo;
import starter.search.SearchFor;
import starter.search.SearchHomepageFor;
import starter.search.SearchResult;

public class UK_VISA_CHECK {

	@Steps
	NavigateTo navigateTo;

	@Steps
	SearchHomepageFor searchHomepageFor;

	@Steps
	SearchFor searchFor;

	@Steps
	SearchResult searchResult;

	@Given("^(?:.*) is on the Check if you need a UK visa page")
	public void i_am_on_the_DuckDuckGo_home_page() {
		navigateTo.theGoogleHomePage();
	}

	@When("^s?he (?:searches|has searched) for \"(.*)\"")
	public void i_search_for(String term) {
		searchHomepageFor.term(term);
	}

	@When("^s?he searches again for \"(.*)\"")
	public void i_search_again_for(String term) {
		// searchFor.term(term);
	}

	@Then("result titles should contain the word {string}")
	public void all_the_result_titles_should_contain_the_word(String expectedTerm) {
		if (expectedTerm.equals("FAILED")) {
			throw new AssertionError("Oh crap!");
		}
		if (expectedTerm.equals("BROKEN")) {
			throw new Error("Oh crap!");
		}
//        assertThat(searchResult.titles())
//                .allMatch(title -> textOf(title).containsIgnoringCase(expectedTerm));
	}

	@Then("^user Select reason for coming to the UK \\\"(.+)\\\"$")
	public void selectReason(String reason) throws InterruptedException {
		searchHomepageFor.reason(reason);
	}

	@Then("^stay Time in UK \\\"(.+)\\\"$")
	public void selectStayTime(String reason) throws InterruptedException {
		if (!(reason.equals("NA"))) {
			searchHomepageFor.reason(reason);
		}
	}

	@Then("^Are you planning to work in any type of job \\\"(.+)\\\"$")
	public void selectJob(String reason) throws InterruptedException {
		if (!(reason.equals("NA"))) {
			searchHomepageFor.reason(reason);
		}
	}

	@Then("^select partner immigration status \\\"(.+)\\\"$")
	public void partnerStatus(String reason) throws InterruptedException {
		if (!(reason.equals("NA"))) {
			searchHomepageFor.reason(reason);
		}
	}

	@Then("^user can see visa not required text \\\"(.+)\\\"$")
	public void visaNotReq(String status) throws InterruptedException {
		searchHomepageFor.visaStatusNotReq(status);
	}
}
