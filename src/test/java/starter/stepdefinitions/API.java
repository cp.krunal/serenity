package starter.stepdefinitions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.xml.XmlPath;
import io.restassured.path.xml.element.NodeChildren;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API {

	private String accessToken;
	public Response response;
	public Map<String, Object> session = new HashMap<String, Object>();

	public String getAccessToken() {
		return accessToken;
	}

	public Map<String, Object> getHeader(String loginTokens) {
		Map<String, Object> headerMap = new HashMap<String, Object>();
//		headerMap.put("Content-Type", "application/json");
		if (loginTokens != null)
			headerMap.put("Authorization", "Bearer " + loginTokens);
		return headerMap;
	}

	public void setAccessToken(Response response) {
		Map<String, Object> responseMap = response.path("");
		accessToken = "Bearer " + responseMap.get("access_token").toString();
	}

	public String getResponseBody(Response response) {
		return response.getBody().asString();
	}

	public int getResponseCode(Response response) {
		return response.getStatusCode();
	}

	public Map<String, Object> getParameter(String grantType, String username, String password) {
		Map<String, Object> headerMap = new HashMap<String, Object>();
		headerMap.put("grant_type", grantType);
		headerMap.put("username", username);
		headerMap.put("password", password);
		return headerMap;
	}

	public RequestSpecification requestSetup(String authorization, boolean contentType) {
		return RestAssured.given().headers(getHeader(authorization, contentType));
	}

	private Map<String, ?> getHeader(String authorization, boolean contentType) {
		Map<String, Object> headerMap = new HashMap<String, Object>();
		headerMap.put("Authorization", authorization);
		if (contentType)
			headerMap.put("Content-Type", ContentType.JSON);
		return headerMap;
	}

	public Response post(RequestSpecification request, String URL) {
		return request.log().all().when().post(URL);
	}

	public Response post(RequestSpecification request, String URL, String body) {
		return request.body(body).log().all().when().post(URL);
	}

	public Response post(RequestSpecification request, String URL, File body) {
		return request.body(body).log().all().when().post(URL);
	}

	public Response get(RequestSpecification request, String URL) {
		return request.log().all().when().get(URL);
	}

	public static void main(String[] args) {
		Response response;
		final String API_KEY = "838cec86-072b-4d84-8166-3ea8464895dd";
		try {
			response = RestAssured.given().accept(ContentType.XML).when().queryParams("res", "3hourly")
					.queryParams("key", API_KEY)
					.get("http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/datatype/sitelist");
			XmlPath xmlBody = response.xmlPath();
			NodeChildren list = xmlBody.get().children();
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getAttribute("id").toString().equals("324152"))
					System.out.println(list.get(i).getAttribute("name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
