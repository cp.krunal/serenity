### Application code
 
These packages generally contain application code. 
If you are writing a reusable test library, you can also place reusable test components such as Page Objects or Tasks here.

Q1. What are Pre-Requisite to execute scripts?
Ans : 1. Chrome version support 90.
	   2. Need maven installed on machine to run test cases through command prompt

Q2. How to run scripts through maven and command prompt?
Ans : 1. Open Command Prompt
	  2. Go to Project location
	  3. Verify maven installed or not mvn -version
	  4. If yes, type command mvn test/ mvn build
